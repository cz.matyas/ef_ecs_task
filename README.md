
# Amazon ECS demo with  AWS CloudFormation


You can launch this CloudFormation stack in your account:


```bash

STACKNAME=ecs-demo
ENV=dev
REGION=us-east-1

aws cloudformation create-stack --stack-name ${STACKNAME} --template-body "file://ecs_demo.yaml"  --parameters file://envirnoment/${ENV}/pamaters.json  --tags file://envirnoment/${ENV}/tags.json     --capabilities CAPABILITY_IAM   --region $REGION
```